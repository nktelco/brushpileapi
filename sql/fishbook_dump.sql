-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: fishbook
-- ------------------------------------------------------
-- Server version	5.5.5-10.6.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sponsor_id` bigint(20) unsigned DEFAULT NULL,
  `image` varchar(1024) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `link` varchar(1024) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sponsor_id` (`sponsor_id`),
  CONSTRAINT `campaign_ibfk_1` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsor` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishingspot`
--

DROP TABLE IF EXISTS `fishingspot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fishingspot` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `lake_id` bigint(20) unsigned DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `state` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lake_id` (`lake_id`),
  CONSTRAINT `fishingspot_ibfk_1` FOREIGN KEY (`lake_id`) REFERENCES `lake` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fishingtip`
--

DROP TABLE IF EXISTS `fishingtip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fishingtip` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `temp_max` int(11) DEFAULT NULL,
  `temp_min` int(11) DEFAULT NULL,
  `precipitation_max` float DEFAULT NULL,
  `precipitation_min` float DEFAULT NULL,
  `season` int(11) DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guide`
--

DROP TABLE IF EXISTS `guide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guide` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `details` text DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `image` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guide_fishingspot`
--

DROP TABLE IF EXISTS `guide_fishingspot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guide_fishingspot` (
  `guide_id` bigint(20) unsigned NOT NULL,
  `fishingspot_id` bigint(20) unsigned DEFAULT NULL,
  KEY `guide_id` (`guide_id`),
  KEY `fishingspot_id` (`fishingspot_id`),
  CONSTRAINT `guide_fishingspot_ibfk_1` FOREIGN KEY (`guide_id`) REFERENCES `guide` (`id`),
  CONSTRAINT `guide_fishingspot_ibfk_2` FOREIGN KEY (`fishingspot_id`) REFERENCES `fishingspot` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lake`
--

DROP TABLE IF EXISTS `lake`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lake` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `displayname` varchar(512) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guide_id` bigint(20) DEFAULT NULL,
  `link` varchar(256) DEFAULT NULL,
  `linktype` varchar(32) DEFAULT NULL,
  UNIQUE KEY `link_un` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sponsor`
--

DROP TABLE IF EXISTS `sponsor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sponsor` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `contact` varchar(512) DEFAULT NULL,
  `contactemail` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `salt` varchar(64) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `weather`
--

DROP TABLE IF EXISTS `weather`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weather` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `location` int(11) DEFAULT NULL,
  `response` text DEFAULT NULL,
  `lastupdate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'fishbook'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: user
# ------------------------------------------------------------
INSERT INTO
`user` (
`id`,
`username`,
`password`,
`salt`,
`role_id`,
`createdOn`,
`active`
)
VALUES
(
2,
'admin',
'c8196d2ce71acbf8f2bd788c34d586cbba2c8b24c59bab3e9f337345d6750bf3',
'fa3b9b6bdcd94424d992fde22fbd6b261752b0ccadf3b483132e23648bfc7420',
9,
'2021-11-29 01:27:20',
1
);

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: lake
# ------------------------------------------------------------
INSERT INTO
`lake` (`id`, `displayname`, `image`, `lat`, `lng`)
VALUES
(3, 'Arkabutla Lake', '', 34.6966, -90.1032);
INSERT INTO
`lake` (`id`, `displayname`, `image`, `lat`, `lng`)
VALUES
(4, 'Bay Springs Lake', '', 34.5748, -88.3139);
INSERT INTO
`lake` (`id`, `displayname`, `image`, `lat`, `lng`)
VALUES
(5, 'Cedar Reservoir', '', 34.5291, -87.9253);

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: fishingspot
# ------------------------------------------------------------

INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
4,
'Arkabutla Lake',
34.6966,
-90.1032,
'',
3,
'Coldwater',
'MS'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
5,
'Bay Springs Lake',
34.5748,
-88.3139,
'',
4,
'Bay Springs',
'MS'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
6,
'Cedar Reservoir',
34.5291,
-87.9253,
'',
5,
'Franklin County',
'AL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
7,
'Clinton Lake',
40.161,
-88.789,
'',
NULL,
'DeWitt',
'IL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
8,
'Dale Hollow Lake',
36.5367,
-85.4516,
'',
NULL,
'Burkesville',
'KY'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
9,
'Decatur Lake',
39.8167,
-88.9333,
'',
NULL,
'Decatur',
'IL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
10,
'Green River Lake',
37.2453,
-85.3394,
'',
NULL,
'Campbellsville',
'KY'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
11,
'Grenada Lake',
33.8059,
-89.771,
'',
NULL,
'Grenada',
'MS'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
12,
'Kentucky Lake',
36.8074,
-88.148,
'',
NULL,
'Kentucky Lake',
'KY'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
13,
'Kerr Reservoir',
36.5985,
-78.2983,
'',
NULL,
'Henderson',
'NC'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
14,
'Kinkaid Lake',
37.8063,
-89.4517,
'',
NULL,
'Jackson',
'IL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
15,
'Lake Belton',
31.1085,
-97.4725,
'',
NULL,
'Lake Belton',
'TX'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
16,
'Lake Cumberland',
36.9592,
-84.7679,
'',
NULL,
'Lake Cumberland',
'KY'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
17,
'Lake Enid',
34.1535,
-89.8051,
'',
NULL,
'Shuford',
'MS'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
18,
'Lake Granger',
30.7184,
-97.3216,
'',
NULL,
'Granger',
'TX'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
19,
'Lake Limestone',
31.3461,
-96.3582,
'',
NULL,
'Thornton',
'TX'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
20,
'Lake Mattoon',
39.3333,
-88.4817,
'',
NULL,
'Mattoon',
'IL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
21,
'Lake of Egypt',
37.5823,
-88.4817,
'',
NULL,
'Little Egypt',
'IL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
22,
'Lake Palestine',
32.1879,
-95.4751,
'',
NULL,
'Henderson',
'TX'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
23,
'Lake Sardis',
34.4789,
-89.647,
'',
NULL,
'Sardis Lake',
'MS'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
24,
'Lake Shelbyville',
39.4067,
-88.7902,
'',
NULL,
'Shelbyville',
'IL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
25,
'Lake Waco',
31.553,
-97.2232,
'',
NULL,
'Waco',
'TX'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
26,
'Little Bear Reservoir',
34.4543,
-87.4756,
'',
NULL,
'Landersville',
'AL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
27,
'Mississippi River',
31.613,
-91.4153,
'',
NULL,
'Pilottown',
'LA'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
28,
'Philpott Lake',
36.8078,
-80.0797,
'',
NULL,
'Philpott Lake',
'VA'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
29,
'Pickwick Lake',
35.0404,
-88.1917,
'',
NULL,
'Pickwick Lake',
'AL'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
30,
'Reelfoot Lake',
36.354,
-89.4131,
'',
NULL,
'Tiptonville',
'TN'
);
INSERT INTO
`fishingspot` (
`id`,
`name`,
`lat`,
`lng`,
`image`,
`lake_id`,
`city`,
`state`
)
VALUES
(
31,
'Rend Lake',
38.0366,
-88.9869,
'',
NULL,
'Benton',
'IL'
);

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: guide
# ------------------------------------------------------------
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
4,
'William Oliver',
'(903) 515-0036',
'',
'',
'',
NULL,
NULL,
'images/ae6cf6b7-e118-4e81-ba14-dcbf679afecf.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
5,
'David Jones',
'(270) 634-2675',
'',
'',
'',
NULL,
NULL,
''
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
6,
'Corey Thomas',
'(270) 566-1152',
'',
'',
'',
NULL,
NULL,
'images/6463583d-71ef-48b3-844d-84cbec909bda.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
7,
'Brian Cleland',
'(217) 412-5941',
'',
'',
'',
NULL,
NULL,
'images/b6225b6a-1074-425d-b9cd-7b0284895d56.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
8,
'Tony Hughes',
'(731) 589-2386',
'tony1967hughes@gmail.com',
'',
'',
NULL,
NULL,
'images/f2f274c5-7f2e-48b2-9815-3ab67be9125e.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
9,
'Joel Harris',
'(662) 424-2551',
'joel@joelharrisfishing.com',
'',
'',
NULL,
NULL,
'images/3fc5f0c4-ea8d-4154-9ccb-be8abbdba3b9.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
10,
'Keith Wray',
'(336) 589-9025',
'',
'',
'',
NULL,
NULL,
'images/704bb183-2b0a-4fd9-b06b-3c8b0b3e555b.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
11,
'John Harrison',
'(662) 983-5999',
'',
'',
'',
NULL,
NULL,
'images/c11b270a-c453-4c9e-974e-ead31ea8729d.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
12,
'Kyle Schoenherr',
'(618) 314-2967',
'',
'',
'',
NULL,
NULL,
'images/3b867d1b-7ff6-433c-9742-0c1537986f17.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
13,
'Warren Cotton',
'(901) 550-6264',
'wcotton56@gmail.com',
'',
'',
NULL,
NULL,
'images/6a1d836b-5f99-4252-b795-c13865ab91b3.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
14,
'Austin Pierce',
'(479) 234-6895',
'',
'',
'',
NULL,
NULL,
'images/3b5b556d-22fd-4c64-a8d8-0ae08e0b0138.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
15,
'Carson Pierce',
'(479) 234-0012',
'',
'',
'',
NULL,
NULL,
'images/e0cf2f2e-ceae-47d6-9412-9a46d048d8c7.jpeg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
16,
'Jason Barton',
'(254) 760-3044',
'',
'',
'',
NULL,
NULL,
'images/6512464e-2ec5-4350-ae7f-424da1904582.jpg'
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(
17,
'Crappie Kirby',
'(913) 461-3752',
'djkirbykc@yahoo.com',
'',
'',
NULL,
NULL,
''
);
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(18, 'Dale Black', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(19, 'Darrell Baker', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(20, 'David Jones', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(21, 'Dean McCoy', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(22, 'Denny Wilbert', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(23, 'Dereck Fulton', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(24, 'Eric Wright', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(25, 'Jake Hengstler', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(26, 'James Mong', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(27, 'Jason Barton', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(28, 'Jason Koesters', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(29, 'Jason Westerberg', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(30, 'Jeff Robl', '', '', '', '', NULL, NULL, '');
INSERT INTO
`guide` (
`id`,
`name`,
`phone`,
`email`,
`bio`,
`details`,
`lat`,
`lng`,
`image`
)
VALUES
(31, 'Jeremy Stoddard', '', '', '', '', NULL, NULL, '');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: guide_fishingspot
# ------------------------------------------------------------
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(4, 22);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(5, 10);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(6, 8);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(6, 16);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(7, 24);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(7, 20);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(7, 9);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(7, 7);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(7, 11);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(7, 23);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(7, 4);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(8, 30);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(8, 12);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(8, 23);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(8, 11);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(9, 5);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(9, 29);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(9, 6);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(9, 26);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(10, 13);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(10, 28);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(11, 4);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(11, 23);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(11, 17);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(11, 11);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(12, 31);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(12, 14);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(12, 21);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(13, 4);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(13, 23);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(13, 17);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(13, 11);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(14, 27);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(15, 27);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(16, 19);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(16, 18);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(16, 25);
INSERT INTO
`guide_fishingspot` (`guide_id`, `fishingspot_id`)
VALUES
(16, 15);

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: link
# ------------------------------------------------------------
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
18,
4,
'https://www.facebook.com/profile.php?id=100010265912360',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
19,
5,
'https://www.facebook.com/davidjonescrappie',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(20, 5, 'www.greenriverlakecrappietrios.com', 'link');
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
21,
6,
'https://www.facebook.com/lakedaysguideservice/',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
22,
7,
'https://www.facebook.com/ClelandOutdoors/',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(23, 8, 'https://tonyhughesfishing.com/', 'link');
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
24,
9,
'https://www.facebook.com/bigunsproguideservice/',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
25,
9,
'https://www.instagram.com/joelharrisfishing/',
'link'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
26,
10,
'https://www.facebook.com/Fishdocs-Guide-Servicecom-880524402317470/',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
27,
10,
'https://www.fishdocsguideservice.com/',
'link'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
28,
11,
'https://www.facebook.com/jhguideservice/',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(29, 12, 'http://www.allseasonscrappie.com/', 'link');
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
30,
13,
'https://www.warrencottonoutdoors.com',
'link'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(31, 14, 'www.t1fishing.com', 'link');
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(32, 15, 'www.t1fishing.com', 'link');
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(
33,
16,
'https://www.facebook.com/cattinaroundadventures',
'facebook'
);
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(34, 16, 'www.cattinaroundadventures.com', 'link');
INSERT INTO
`link` (`id`, `guide_id`, `link`, `linktype`)
VALUES
(35, 17, 'www.crappiekirby.com', 'link');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: sponsor
# ------------------------------------------------------------
INSERT INTO
`sponsor` (`id`, `name`, `contact`, `contactemail`)
VALUES
(4, 'Sponsor 1', 'Jim Jimson', 'jim@jim.com');
INSERT INTO
`sponsor` (`id`, `name`, `contact`, `contactemail`)
VALUES
(5, 'Sponsor 2', 'Bill Billson', 'bill@bill.com');


# ------------------------------------------------------------
# DATA DUMP FOR TABLE: campaign
# ------------------------------------------------------------
INSERT INTO
`campaign` (
`id`,
`sponsor_id`,
`image`,
`text`,
`link`,
`startdate`,
`enddate`
)
VALUES
(
7,
4,
'images/b22d6646-b591-4ffb-ab43-e6fb879b8704.jpg',
'Get Some Pizza!',
'https://pizzahut.com',
NULL,
NULL
);
INSERT INTO
`campaign` (
`id`,
`sponsor_id`,
`image`,
`text`,
`link`,
`startdate`,
`enddate`
)
VALUES
(
8,
5,
'images/51fda35d-a4ca-4b0c-8cfc-216bc85fcde3.jpg',
'Whatever this is',
'https://rollerblades.com',
NULL,
NULL
);

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: fishingtip
# ------------------------------------------------------------
# ------------------------------------------------------------
# DATA DUMP FOR TABLE: weather
# ------------------------------------------------------------
