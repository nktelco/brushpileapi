# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.20)
# Database: fishbook
# Generation Time: 2021-10-08 04:47:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table campaign
# ------------------------------------------------------------

CREATE TABLE `campaign` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `sponsor_id` bigint unsigned DEFAULT NULL,
  `image` varchar(1024) DEFAULT NULL,
  `text` text,
  `link` varchar(1024) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sponsor_id` (`sponsor_id`),
  CONSTRAINT `campaign_ibfk_1` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table fishingspot
# ------------------------------------------------------------

CREATE TABLE `fishingspot` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `lake_id` bigint unsigned DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `state` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lake_id` (`lake_id`),
  CONSTRAINT `fishingspot_ibfk_1` FOREIGN KEY (`lake_id`) REFERENCES `lake` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table fishingtip
# ------------------------------------------------------------

CREATE TABLE `fishingtip` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `temp_max` int DEFAULT NULL,
  `temp_min` int DEFAULT NULL,
  `precipitation_max` float DEFAULT NULL,
  `precipitation_min` float DEFAULT NULL,
  `season` int DEFAULT NULL,
  `text` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table guide
# ------------------------------------------------------------

CREATE TABLE `guide` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `bio` text,
  `details` text,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `image` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table guide_fishingspot
# ------------------------------------------------------------

CREATE TABLE `guide_fishingspot` (
  `guide_id` bigint unsigned NOT NULL,
  `fishingspot_id` bigint unsigned DEFAULT NULL,
  KEY `guide_id` (`guide_id`),
  KEY `fishingspot_id` (`fishingspot_id`),
  CONSTRAINT `guide_fishingspot_ibfk_1` FOREIGN KEY (`guide_id`) REFERENCES `guide` (`id`),
  CONSTRAINT `guide_fishingspot_ibfk_2` FOREIGN KEY (`fishingspot_id`) REFERENCES `fishingspot` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table lake
# ------------------------------------------------------------

CREATE TABLE `lake` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `displayname` varchar(512) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table sponsor
# ------------------------------------------------------------

CREATE TABLE `sponsor` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `contact` varchar(512) DEFAULT NULL,
  `contactemail` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table user
# ------------------------------------------------------------

CREATE TABLE `user` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `salt` varchar(64) DEFAULT NULL,
  `role_id` bigint DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT NULL,
  `active` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



# Dump of table weather
# ------------------------------------------------------------

CREATE TABLE `weather` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `location` int DEFAULT NULL,
  `response` text,
  `lastupdate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
