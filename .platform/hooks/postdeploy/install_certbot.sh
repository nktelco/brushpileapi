#!/bin/bash

sudo wget -r --no-parent -A 'epel-release-*.rpm' https://archives.fedoraproject.org/pub/archive/epel/7/x86_64/Packages/e/
sudo rpm -Uvh archives.fedoraproject.org/pub/archive/epel/7/x86_64/Packages/e/epel-release-*.rpm --force
sudo yum-config-manager --enable epel*
sudo yum install -y certbot-nginx
sudo certbot --nginx --non-interactive --email ben@pxlplz.com --agree-tos --domains bpfapi.com --keep-until-expiring
ln -sf /etc/letsencrypt/live/${DOMAIN_LINK} /etc/letsencrypt/live/ebcert
touch /etc/cron.d/certbot_renew
chmod +x /etc/cron.d/certbot_renew
echo "@weekly root certbot renew\n" >> /etc/cron.d/certbot_renew