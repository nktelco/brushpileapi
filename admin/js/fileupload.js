const FileUpload = (file, route) => {
    return new Promise((resolve, reject) => {
        let formData = new FormData();
        formData.append("file", file);

        $.ajax({
            url: '/fishingspot/image',
            data: formData,
            processData: false,
            type: 'POST',

            // This will override the content type header,
            // regardless of whether content is actually sent.
            // Defaults to 'application/x-www-form-urlencoded'
            //contentType: 'multipart/form-data',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)

            //Before 1.5.1 you had to do this:
            beforeSend: (x) => {
                if (x && x.overrideMimeType) {
                    x.overrideMimeType("multipart/form-data");
                }
            },
            // Now you should be able to do this:
            mimeType: 'multipart/form-data',    //Property added in 1.5.1

            success: (data) => {
                resolve(data)
            },
            error: (e) => {
                console.log("error sending file");
                console.log(e);
                reject(e)
            }
        });
    })
}
