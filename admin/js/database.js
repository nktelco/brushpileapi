$().ready(() => {

    $("#deletedb").click(() => {
        $("#droptablemodal").css("visibility", "visible");
        $("#modalcontainer").css("visibility", "visible");
    });

    $("#dropdatabaseok").click(() => {
        $.get('/setup/recreate').then(() => {
            window.location.replace("/logout");
        }).catch((e) => {
            window.location.replace("/logout");
        })
    });

    $("#dbrestore").click(() => {
        const sqlfile = $("#sqlfile")[0].files[0];
        let newFileName;
        UploadSql(sqlfile).then((response) => {
            alert("Database Updated");
        }).catch((e) => {
            alert("Database Error");
        });
    })

    $("#dropdatabasecancel").click(() => {
        $("#droptablemodal").css("visibility", "hidden");
        $("#modalcontainer").css("visibility", "hidden");
    });

    const UploadSql = (file) => {
        return new Promise((resolve, reject) => {
            if(file) {
                let formData = new FormData();
                formData.append("file", file);

                $.ajax({
                    url: '/setup/upload',
                    data: formData,
                    processData: false,
                    type: 'POST',

                    // This will override the content type header,
                    // regardless of whether content is actually sent.
                    // Defaults to 'application/x-www-form-urlencoded'
                    //contentType: 'multipart/form-data',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)

                    //Before 1.5.1 you had to do this:
                    beforeSend: (x) => {
                        if (x && x.overrideMimeType) {
                            x.overrideMimeType("multipart/form-data");
                        }
                    },
                    // Now you should be able to do this:
                    mimeType: 'multipart/form-data',    //Property added in 1.5.1

                    success: (data) => {
                        resolve(data)
                    },
                    error: (e) => {
                        console.log("error sending file");
                        console.log(e);
                        reject(e)
                    }
                });
            } else if(currentGuide.image) {
                resolve(currentGuide.image);
            } else {
                resolve(null);
            }
        })
    }
});