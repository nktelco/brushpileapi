$().ready(() => {
    $("#addrow").click(() => {
        addLake();
    });

    $("#newbuttoncancel").click(() => {
        hidenewrow();
    });

    $("#newbuttonok").click(() => {
       submitNewLake();
    });

    GetAllLakes();
});

const UploadImage = (file) => {
    return new Promise((resolve, reject) => {
        let formData = new FormData();
        formData.append("file", file);

        $.ajax({
            url: '/lake/image',
            data: formData,
            processData: false,
            type: 'POST',

            // This will override the content type header,
            // regardless of whether content is actually sent.
            // Defaults to 'application/x-www-form-urlencoded'
            //contentType: 'multipart/form-data',
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)

            //Before 1.5.1 you had to do this:
            beforeSend: (x) => {
                if (x && x.overrideMimeType) {
                    x.overrideMimeType("multipart/form-data");
                }
            },
            // Now you should be able to do this:
            mimeType: 'multipart/form-data',    //Property added in 1.5.1

            success: (data) => {
                resolve(data)
            },
            error: (e) => {
                console.log("error sending file");
                console.log(e);
                reject(e)
            }
        });
    })
}

const PostNewLakeData = (lakedata) => {

    return new Promise((resolve, reject) => {
        $.post('/lake', lakedata).then((result) => {
            resolve();
        }).catch((e) => {
            reject(e);
        })
    })
}

const submitNewLake = () => {
    const newlakename = $("#newlakename").val();
    const newimage = $("#newimage")[0].files[0];
    let newimagename = "";
    const newlat = $("#newlat").val();
    const newlng = $("#newlng").val();

    UploadImage(newimage).then((result) => {
        newimagename = result;
        return(PostNewLakeData({
            displayname: newlakename,
            image: newimagename,
            lat: newlat,
            lng: newlng
        }));

    }).then((result) => {
        document.getElementById("modalcontainer").style.visibility = "hidden";
        document.getElementById("addrowmodal").style.visibility = "hidden";
        ClearLakes();
        GetAllLakes();

    }).catch((e) => {
        alert(e.statusText);
    })
}

const deleteLakeById = (id) => {
    alert("deleting lake " + id);
}

const modifyLakeById = (lakeid) => {
    console.log(lakeid);
    document.getElementById("modalcontainer").style.visibility = "visible";
    document.getElementById("addrowmodal").style.visibility = "visible";
}

const addLake = () => {
    document.getElementById("modalcontainer").style.visibility = "visible";
    document.getElementById("addrowmodal").style.visibility = "visible";
}

const hidenewrow = () => {
    document.getElementById("modalcontainer").style.visibility = "hidden";
    document.getElementById("addrowmodal").style.visibility = "hidden";
}

const ClearLakes = () => {
    document.getElementById("infotablebody").innerHTML = "";
}

const GetAllLakes = () => {

    $.get("/lake").then((results) => {
        const trs = results.map((item, index) => {
            const tableElement = document.createElement("tr");

            addTextToTable(item.id, tableElement)
            addTextToTable(item.displayname, tableElement)
            addImageToTable(item.image, tableElement)
            addTextToTable(item.lat, tableElement)
            addTextToTable(item.lng, tableElement)
            addLinkToTable("[edit]", () => { modifyLakeById(item.id) }, "#", tableElement);
            addLinkToTable("X", () => { deleteLakeById(item.id) }, "#", tableElement);
            return tableElement;
        })

        for(let tr of trs) {
            document.getElementById("infotablebody").appendChild(tr);
        }

    }).catch((e) => {
        console.log(e);
    })
}
