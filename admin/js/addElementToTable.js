const addLinkToTable = (text, onClickEvent, linkhref, tableElement) => {
    const link = document.createElement("a");
    link.href = linkhref;
    link.onclick = onClickEvent;
    link.innerText = text;

    const td = document.createElement("td");
    td.appendChild(link);

    tableElement.appendChild(td);
}

const addImageToTable = (imgsrc, tableElement) => {
    const image = document.createElement("td");
    const imageLink = document.createElement("img");
    imageLink.src = imgsrc ? "../" + imgsrc : 'img/SVG/user.svg';
    image.appendChild(imageLink);

    tableElement.appendChild(image);
}

const addTextToTable = (text, tableElement) => {
    const textElement = document.createElement("td");
    textElement.innerText = text || "n/a";

    tableElement.appendChild(textElement);
}
