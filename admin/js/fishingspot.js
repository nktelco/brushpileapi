$().ready(() => {
    $("#addrow").click(() => {
        addFishingSpot();
    });

    $("#newbuttoncancel").click(() => {
        hidenewrow();
    });

    $("#newbuttonok").click(() => {
        submitNewFishingSpot();
    });

    GetAllFishingSpots();
    GetAllLakes();
});

let currentFishingSpot = {};

const UploadImage = (file) => {
    return FileUpload(file, '/fishingspot/image');
}

const PostNewFishingSpotData = (fishingspotdata) => {

    return new Promise((resolve, reject) => {
        $.post('/fishingspot', fishingspotdata).then((result) => {
            resolve();
        }).catch((e) => {
            reject(e);
        })
    })
}

const PatchNewFishingSpotData = (fishingspotdata) => {
    const {id} = fishingspotdata;
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/fishingspot/${id}`,
            data: fishingspotdata,
            method: "PATCH"
        }).then((result) => {
            resolve();
        }).catch((e) => {
            reject();
        })
    })
}

const populateFishingSpotRows = (fishingSpot) => {
    const {id, name, lat, lng, image, lake_id, city, state, lakename} = fishingSpot;
    $("#newname").val(name || "");
    $("#newlat").val(lat || "");
    $("#newlng").val(lng || "");
    $("#newlake").val(lake_id || "");
    $("#newcity").val(city || "");
    $("#newstate").val(state || "");
}

const submitNewFishingSpot = () => {
    const newname = $("#newname").val();
    const newlat = $("#newlat").val();
    const newlng = $("#newlng").val();
    const newimage = $("#newimage")[0].files[0];
    let newimagename = "";
    const newlake = $("#newlake").val();
    const newcity = $("#newcity").val();
    const newstate = $("#newstate").val();

    UploadImage(newimage).then((result) => {
        newimagename = result;
        if(currentFishingSpot.id) {
            return (PatchNewFishingSpotData({
                name: newname,
                lat: newlat,
                lng: newlng,
                image: newimagename,
                lake_id: newlake,
                city: newcity,
                state: newstate,
                id: currentFishingSpot.id,
            }))
        } else {
            return (PostNewFishingSpotData({
                name: newname,
                lat: newlat,
                lng: newlng,
                image: newimagename,
                lake_id: newlake,
                city: newcity,
                state: newstate
            }));
        }
    }).then((result) => {
        document.getElementById("modalcontainer").style.visibility = "hidden";
        document.getElementById("addrowmodal").style.visibility = "hidden";
        ClearFishingSpots();
        GetAllFishingSpots();
        currentFishingSpot = {};

    }).catch((e) => {
        console.log(e);
        alert(e.statusText);
    })
}

const deleteLakeById = (id) => {
    alert("deleting lake " + id);
}

const modifyFishingSpotById = (fishingspotId) => {
    $.get(`/fishingspot/${fishingspotId}`).then((result) => {


        document.getElementById("modalcontainer").style.visibility = "visible";
        document.getElementById("addrowmodal").style.visibility = "visible";

        currentFishingSpot = result[0];
        populateFishingSpotRows(currentFishingSpot);
    })
}

const addFishingSpot = () => {
    document.getElementById("modalcontainer").style.visibility = "visible";
    document.getElementById("addrowmodal").style.visibility = "visible";
}

const hidenewrow = () => {
    document.getElementById("modalcontainer").style.visibility = "hidden";
    document.getElementById("addrowmodal").style.visibility = "hidden";
}

const ClearFishingSpots = () => {
    document.getElementById("infotablebody").innerHTML = "";
}

const GetAllLakes = () => {
    const lakeSelectDropdown = document.getElementById("newlake");
    $.get("/lake").then((results) => {

        const option = document.createElement("option");
        option.innerHTML = "select a lake";
        option.value = null;
        option.selected = true;
        option.disabled = true;
        option.hidden = true;
        lakeSelectDropdown.appendChild(option);


        for(const result of results) {
            const option = document.createElement("option");
            option.innerHTML = result.displayname;
            option.value = result.id;
            lakeSelectDropdown.appendChild(option);
        }
    })
}

const GetAllFishingSpots = () => {

    $.get("/fishingspot").then((results) => {
        const trs = results.map((item, index) => {
            const tableElement = document.createElement("tr");

            addTextToTable(item.id, tableElement)
            addTextToTable(item.name, tableElement)
            addTextToTable(item.lat, tableElement)
            addTextToTable(item.lng, tableElement)
            addImageToTable(item.image, tableElement)
            addTextToTable(item.lakename, tableElement)
            addTextToTable(item.city, tableElement)
            addTextToTable(item.state, tableElement)

            addLinkToTable("[edit]", () => { modifyFishingSpotById(item.id) }, "#", tableElement);
            addLinkToTable("X", () => { deleteLakeById(item.id) }, "#", tableElement);
            return tableElement;
        })

        for(let tr of trs) {
            document.getElementById("infotablebody").appendChild(tr);
        }

    }).catch((e) => {
        console.log(e);
    })
}
