$().ready(() => {
    getAllSponsors();
    getAllCampaigns();

    $("#addrow").click(() => {
        addCampaign();
    });

    $("#newbuttoncancel").click(() => {
        cancelAddCampaign();
    });

    $("#newbuttonok").click(() => {
        addRow();
    });

    $("#newstartdate").datepicker({format: "mm-dd-yyyy", });
    $("#newenddate").datepicker({format: "mm-dd-yyyy", });
    $("#clearstartdate").click(() => {
        $("#newstartdate").val("");
    });
    $("#clearenddate").click(() => {
        $("#newenddate").val("");
    });

})

let currentCampaign = {};
let sponsors = [];

getAllCampaigns = () => {
    $.get('/campaign').then((results) => {
        const trs = results.map((item, index) => {
            const tableElement = document.createElement("tr");

            addTextToTable(item.id, tableElement);
            addTextToTable(item.sponsor_name, tableElement);
            addImageToTable(item.image, tableElement);
            addTextToTable(item.text, tableElement);
            addTextToTable(item.link, tableElement);
            addTextToTable(item.startdate ? new Date(item.startdate).toDateString() : null, tableElement);
            addTextToTable(item.enddate ? new Date(item.enddate).toDateString() : null, tableElement);
            addLinkToTable("[edit]", () => { modifyCampaignById(item.id) }, "#", tableElement);
            addLinkToTable("X", () => { deleteCampaignById(item.id) }, "#", tableElement);
            return tableElement;
        })

        for(let tr of trs) {
            document.getElementById("infotablebody").appendChild(tr);
        }
    }).catch((e) => {
        console.log(e);
    })
}

modifyCampaignById = (id) => {
    $.get(`/campaign/${id}`).then((result) => {
        if(result.length > 0) {
            currentCampaign = result[0];
            addCampaign();

            $("#newsponsor").val(currentCampaign.sponsor_id);
            $("#newtext").val(currentCampaign.text);
            $("#newlink").val(currentCampaign.link);
            $("#newstartdate").val(currentCampaign.startdate || "");
            $("#newenddate").val(currentCampaign.enddate || "");
        }
    })
}

deleteCampaignById = (id) => {
    $.ajax({url: `/campaign/${id}`, method: "DELETE"}).then((result) => {
        clearCampaigns();
        getAllCampaigns();
    }).catch((e) => {
        alert("couldn't delete");
    })
}

getAllSponsors = () => {
    const sponsorList = document.getElementById("newsponsor");

    $.get('/sponsor').then((result) => {
        sponsors = result;

        for(let sp of sponsors) {
            const opt = document.createElement("OPTION");
            opt.value = sp.id;
            opt.innerText = sp.name;
            sponsorList.appendChild(opt);
        }
    }).catch((e) => {
        console.log(e);
    })
}

clearCampaigns = () => {
    document.getElementById("infotablebody").innerHTML = "";
}

addRow = () => {
    const sponsor_id = $("#newsponsor").val();
    const image = $("#newimage").prop('files')[0] || null;
    const text = $("#newtext").val();
    const link = $("#newlink").val();
    const startdate = $("#newstartdate").val() || null;
    const enddate = $("#newenddate").val() || null;
    const id = currentCampaign.id;

    const data = {sponsor_id, text, link, startdate, enddate, id};
    uploadImage(image).then((results) => {
        data.image = results;
        return postOrPatchRow(data);
    }).then((result) => {
        cancelAddCampaign();
        clearCampaigns();
        getAllCampaigns();
    }).catch((e) => {
        console.log(e);
    })
}

uploadImage = (image) => {
    if(image) {
        return FileUpload(image, '/campaign/image')
    } else if(currentCampaign.image) {
        return Promise.resolve(currentCampaign.image);
    } else {
        return Promise.resolve(null);
    }
}

patchCampaignData = (data, id) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/campaign/${id}`,
            data: data,
            method: "PATCH"
        }).then((result) => {
            resolve(result);
        }).catch((e) => {
            reject(e);
        })
    })
}

postCampaignData = (data) => {
    return new Promise((resolve, reject) => {
        $.post('/campaign', data).then((result) => {
            resolve(result);
        }).catch((e) => {
            reject(e.message);
        })
    })
}

postOrPatchRow = (data) => {
    return new Promise((resolve, reject) => {
        if(data.id) {
            patchCampaignData(data, data.id).then((result) => {
                resolve(result)
            }).catch((e) => {
                reject(e)
            })
        } else {
            postCampaignData(data).then((result) => {
                resolve(result);
            }).catch((e) => {
                reject(e);
            })
        }
    });
}

const clearRows = () => {
    $("#newsponsor").val("");
    $("#newtext").val("");
    $("#newlink").val("");
    $("#newstartdate").val("");
    $("#newenddate").val("");
};

addCampaign = () => {
    clearRows();
    document.getElementById("modalcontainer").style.visibility = "visible";
    document.getElementById("addrowmodal").style.visibility = "visible";
}

cancelAddCampaign = () => {
    currentCampaign = {}
    document.getElementById("modalcontainer").style.visibility = "hidden";
    document.getElementById("addrowmodal").style.visibility = "hidden";
}
