$().ready(() => {
    $("#addrow").click(() => {
        addSponsor();
    });

    $("#newbuttoncancel").click(() => {
        hidenewrow();
    });

    $("#newbuttonok").click(() => {
        submitNewSponsor();
    });

    GetAllSponsors();
});

let currentSponsor = {};
const GetAllSponsors = () => {

    $.get("/sponsor").then((results) => {
        const trs = results.map((item, index) => {
            const tableElement = document.createElement("tr");

            addTextToTable(item.id, tableElement)
            addTextToTable(item.name, tableElement)
            addTextToTable(item.contact, tableElement)
            addTextToTable(item.contactemail, tableElement)
            addLinkToTable("[edit]", () => { modifySponsorById(item.id) }, "#", tableElement);
            addLinkToTable("X", () => { deleteSponsorById(item.id) }, "#", tableElement);
            return tableElement;
        })

        for(let tr of trs) {
            document.getElementById("infotablebody").appendChild(tr);
        }

    }).catch((e) => {
        console.log(e);
    })
}

const modifySponsorById = (id) => {
    $.get(`/sponsor/${id}`).then((results) => {
        if(results.length > 0) {
            currentSponsor = results[0];
            document.getElementById("modalcontainer").style.visibility = "visible";
            document.getElementById("addrowmodal").style.visibility = "visible";

            $("#newname").val(currentSponsor.name || "");
            $("#newcontact").val(currentSponsor.contact || "");
            $("#newcontactemail").val(currentSponsor.contactemail || "");
        }
    }).catch((e) => {
        alert(e.message);
    })
}

const deleteSponsorById = (id) => {

}

const postNewSponsorData = (data) => {

    return new Promise((resolve, reject) => {
        $.post('/sponsor', data).then((result) => {
            resolve(result);
        }).catch((e) => {
            reject(e.message);
        })
    })
}

const patchSponsorData = (data, id) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/sponsor/${id}`,
            data: data,
            method: "PATCH"
        }).then((result) => {
            resolve(result);
        }).catch((e) => {
            reject(e);
        })
    })
}

const hidenewrow = () => {
    currentSponsor = {};
    document.getElementById("modalcontainer").style.visibility = "hidden";
    document.getElementById("addrowmodal").style.visibility = "hidden";
}

const clearRows = () => {
    $("#newname").val("");
    $("#newcontact").val("");
    $("#newcontactemail").val("");
};

const addSponsor = () => {
    clearRows();
    document.getElementById("modalcontainer").style.visibility = "visible";
    document.getElementById("addrowmodal").style.visibility = "visible";
}

const ClearSponsors = () => {
    document.getElementById("infotablebody").innerHTML = "";
}

const postOrPatchRow = () => {
    const newname = $("#newname").val();
    const newcontact = $("#newcontact").val();
    const newcontactemail = $("#newcontactemail").val();

    return new Promise((resolve, reject) => {
        if(currentSponsor.id) {
            patchSponsorData({
                id: currentSponsor.id,
                name: newname,
                contact: newcontact,
                contactemail: newcontactemail
            }, currentSponsor.id).then((result) => {
                resolve(result)
            }).catch((e) => {
                reject(e)
            })
        } else {
            postNewSponsorData({
                name: newname,
                contact: newcontact,
                contactemail: newcontactemail
            }).then((result) => {
                resolve(result);
            }).catch((e) => {
                reject(e);
            })
        }
    })
}

const submitNewSponsor = () => {
    postOrPatchRow().then((result) => {
        document.getElementById("modalcontainer").style.visibility = "hidden";
        document.getElementById("addrowmodal").style.visibility = "hidden";
        ClearSponsors();
        GetAllSponsors();
        currentSponsor = {};
    }).catch((e) => {
        console.log(e);
    })
}
