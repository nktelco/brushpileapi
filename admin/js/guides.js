$().ready(() => {
    $("#addrow").click(() => {
        addGuide();
    });

    $("#addsocialrow").click(() => {
        addSocialRow();
    })

    $("#newbuttoncancel").click(() => {
        hidenewrow();
    });

    $("#socialmediabuttoncancel").click(() => {
        hideSocialMedia();
    });

    $("#newbuttonok").click(() => {
        submitNewGuide();
    });

    $("#socialmediabuttonok").click(() => {
        submitSocialMediaChange();
    });

    $("#addservicearea").click(() => {
        if(currentGuide.fishingSpots == null) {
            currentGuide.fishingSpots = [];
        }
        const newservicearea = document.getElementById("newservicearea").value;
        const servicearealist = document.getElementById("servicearealist");

        const filteredSpot = fishingSpots.filter((spot) => {
            return spot.id == newservicearea;
        })

        if(filteredSpot.length == 1) {
            currentGuide.fishingSpots.push({...filteredSpot[0], new: true});
        }

        //if we're creating a new guide
        addValuesToCurrentGuide();
        setupGuideToEdit();
    })

    GetAllGuides();
    GetAllFishingSpots();
});

let currentGuide = [];
let fishingSpots = [];
let deleteFishingSpotIdList = [];
let guidelist = [];

const addValuesToCurrentGuide = () => {
    const name = $("#newname").val();
    const phone = $("#newphone").val();
    const email = $("#newemail").val();
    const bio = $("#newbio").val();
    const details = $("#newdetails").val();
    const lat = $("#newlat").val();
    const lng = $("#newlng").val();

    const image = $("#newimage")[0].files[0];
    currentGuide = {...currentGuide, name, phone, email, bio, details, lat, lng};
}

const GetAllFishingSpots = () => {
    $.get("/fishingspot").then((results) => {
        fishingSpots = results;
        const newservicearea = document.getElementById("newservicearea");

        for(const spot of fishingSpots) {
            const option = document.createElement("option");
            option.innerHTML = spot.name;
            option.value = spot.id;

            newservicearea.appendChild(option);
        }

    }).catch((e) => {

    })
}

const UploadImage = (file) => {
    return new Promise((resolve, reject) => {
        if(file) {
            let formData = new FormData();
            formData.append("file", file);

            $.ajax({
                url: '/guide/image',
                data: formData,
                processData: false,
                type: 'POST',

                // This will override the content type header,
                // regardless of whether content is actually sent.
                // Defaults to 'application/x-www-form-urlencoded'
                //contentType: 'multipart/form-data',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)

                //Before 1.5.1 you had to do this:
                beforeSend: (x) => {
                    if (x && x.overrideMimeType) {
                        x.overrideMimeType("multipart/form-data");
                    }
                },
                // Now you should be able to do this:
                mimeType: 'multipart/form-data',    //Property added in 1.5.1

                success: (data) => {
                    resolve(data)
                },
                error: (e) => {
                    console.log("error sending file");
                    console.log(e);
                    reject(e)
                }
            });
        } else if(currentGuide.image) {
            resolve(currentGuide.image);
        } else {
            resolve(null);
        }
    })
}

const PostNewGuideData = (guidedata) => {
    return new Promise((resolve, reject) => {
        $.post('/guide', guidedata).then((result) => {
            resolve(result);
        }).catch((e) => {
            reject(e);
        })
    })
}

const PatchNewGuideData = (guidedata, id) => {
    return new Promise((resolve, reject) => {
        $.ajax({url: `/guide/${id}`, data: guidedata, method: "PATCH"}).then((result) => {
            resolve(result);
        }).catch((e) => {
            reject(e);
        })
    })
}

const submitSocialMediaChange = async() => {
    document.getElementById("blocker").style.visibility = "visible";

    for(const r of currentGuide) {
        if(r.updated) {
            const link_id = await postOrPatchRow(r);
            r.link_id = link_id;
            r.updated = false;
        }
    }

    window.setTimeout(() => {
        document.getElementById("blocker").style.visibility = "hidden";
    }, 1000);
}

const postFishingSpotAssociation = (fishingSpot) => {
    return new Promise((resolve, reject) => {
        $.post(`/guide/fishingspot`, fishingSpot).then((result) => {
            resolve();
        }).catch((e) => {
            reject();
        })
    });
}

const postOrPatchRow = (r) => {
    return new Promise((resolve, reject) => {
        if(r.link_id) {
            $.ajax({
                url: `/guide/link/${r.link_id}`,
                data: {
                    link: r.link,
                    linktype: r.linktype
                },
                method: "PATCH"
            }).then((result) => {
                console.log(result);
                resolve(result)
            }).catch((e) => {
                console.log(e);
                reject(e);
            })

        } else {
            console.log(r);
            $.post('/guide/link', {
                guide_id: r.id,
                link: r.link,
                linktype: r.linktype
            }).then((result) => {
                resolve(result)
            }).catch((e) => {
                reject(e);
            })
        }
    })
}

const submitNewGuide = () => {
    const newname = $("#newname").val();
    const newphone = $("#newphone").val();
    const newemail = $("#newemail").val();
    const newbio = $("#newbio").val();
    const newdetails = $("#newdetails").val();
    const newlat = $("#newlat").val();
    const newlng = $("#newlng").val();

    const newimage = $("#newimage")[0].files[0];
    let newimagename = "";

    UploadImage(newimage).then((result) => {
        newimagename = result;
        if (currentGuide.id) {
            return (PatchNewGuideData({
                name: newname,
                phone: newphone,
                email: newemail,
                bio: newbio,
                details: newdetails,
                lat: newlat,
                lng: newlng,
                image: newimagename
            }, currentGuide.id));
        } else {
            return (PostNewGuideData({
                name: newname,
                phone: newphone,
                email: newemail,
                bio: newbio,
                details: newdetails,
                lat: newlat,
                lng: newlng,
                image: newimagename
            }));
        }
    }).then((result) => {
        currentGuide.id = result;
        return (deleteAllServiceAreasInList())
    }).then((result) => {
        return (PostNewServiceAreas(currentGuide.id))
    }).then((result) => {
        document.getElementById("modalcontainer").style.visibility = "hidden";
        document.getElementById("addrowmodal").style.display = "none";
        ClearGuides();
        GetAllGuides();

    }).catch((e) => {
        console.log(e);
        alert(e.statusText);
    })
}

const PostNewServiceAreas = async (currentguide_id) => {
    return new Promise(async(resolve, reject) => {
        for (const f of currentGuide.fishingSpots) {
            if(f.new) {
                await postFishingSpotAssociation({
                    fishingSpotId: f.id,
                    guideId: currentguide_id
                });
            }
        }

        resolve();
    })
}

const addToDeleteServiceAreaQueue = (guideId, fishingSpotId) => {
    deleteFishingSpotIdList.push({guideId: guideId, fishingSpotId: fishingSpotId});
    console.log(currentGuide.fishingSpots);
    currentGuide.fishingSpots = currentGuide.fishingSpots.filter((spot) => (
        spot.id != fishingSpotId
    ))

    setupGuideToEdit();
}

const deleteAllServiceAreasInList = async() => {
    return new Promise(async(resolve, reject) => {
        for (let item of deleteFishingSpotIdList) {
            await deleteServiceArea(item.guideId, item.fishingSpotId);
        }

        deleteFishingSpotIdList = [];
        resolve();
    })
}

const deleteServiceArea = (guideId, fishingSpotId) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `/guide/fishingspot/${guideId}/${fishingSpotId}`,
            method: "DELETE"
        }).then((result) => {
            resolve(result);
        }).catch((e) => {
            reject(e);
        })
    })
}

const setupGuideToEdit = () => {
    const {name, phone, email, bio, details, lat, lng, fishingSpots, id} = currentGuide;
    const guideId = id;

    servicearealist.innerHTML = "";
    for(const spot of fishingSpots || []) {
        const spotId = spot.id;
        if(spotId) {
            const newspot = document.createElement("li");
            newspot.innerHTML = `<span class="left">${spot.name}</span>
                             <span class="right">
                                <a href="#" onclick="addToDeleteServiceAreaQueue(${guideId}, ${spotId})">x</a>
                             </span>`;
            servicearealist.appendChild(newspot);
        }
    }

    $("#newname").val(name || "");
    $("#newphone").val(phone || "");
    $("#newemail").val(email || "");
    $("#newbio").val(bio || "");
    $("#newdetails").val(details || "");
    $("#newlat").val(lat || "");
    $("#newlng").val(lng || "");
}

const deleteGuideById = (id) => {
    alert("deleting guide " + id);
}

const modifyGuideById = (guideid) => {

    $.get(`/guide/${guideid}/fishingspot`).then((result) => {
        console.log(result);
        currentGuide = result[0];
        setupGuideToEdit();

        document.getElementById("modalcontainer").style.visibility = "visible";
        document.getElementById("addrowmodal").style.display = null;
    })
}

const addGuide = () => {
    currentGuide.fishingSpots = [];
    currentGuide = {};
    setupGuideToEdit();
    document.getElementById("modalcontainer").style.visibility = "visible";
    document.getElementById("addrowmodal").style.display = null;
}

const hidenewrow = () => {
    currentGuide.fishingSpots = [];
    currentGuide = {};
    setupGuideToEdit();
    document.getElementById("modalcontainer").style.visibility = "hidden";
    document.getElementById("addrowmodal").style.display = "none";
}

const ClearGuides = () => {
    const guideTableBody = document.getElementById("infotablebody");
    if(guideTableBody) {
        guideTableBody.innerHTML = "";
    }
}

const addSocialRow = () => {
    currentGuide.push({});
    populateSocialMediaRows();
}

const populateSocialMediaRows = () => {
    const socialMediaList = document.getElementById("socialmedialist");
    socialMediaList.innerHTML = "";

    for(let i=0; i<currentGuide.length; i++) {
        const r = currentGuide[i];

        //populate the guide_id with the id
        currentGuide[i].id = currentGuide[0].id;

        const link = r.link || "";
        const type = r.linktype;
        const div = document.createElement("div");
        const linkInput = document.createElement("input");
        linkInput.type = "text";
        linkInput.value = link;
        linkInput.onblur = (e) => {
            currentGuide[i]['link'] = e.target.value;
            currentGuide[i]['updated'] = true;
        }
        div.appendChild(linkInput);

        const linkDropdown = document.createElement("select");
        linkDropdown.onblur = (e) => {
            currentGuide[i]['linktype'] = e.target.value;
            currentGuide[i]['updated'] = true;
        }

        const optionFacebook = document.createElement("option");
        optionFacebook.innerHTML = "Facebook";
        optionFacebook.value = "facebook";
        linkDropdown.append(optionFacebook);

        const optionTwitter = document.createElement("option");
        optionTwitter.innerHTML = "Twitter";
        optionTwitter.value = "value";
        linkDropdown.append(optionTwitter);

        const optionWebsite = document.createElement("option");
        optionWebsite.innerHTML = "Link";
        optionWebsite.value = "link";
        linkDropdown.append(optionWebsite);

        div.appendChild(linkDropdown);
        socialMediaList.appendChild(div);
        linkDropdown.value = type;
    }
}

const viewSocialMedia = (id) => {
    $.get(`/guide/${id}`).then((result) => {
        currentGuide = result;
        populateSocialMediaRows();
    })

    document.getElementById("modalcontainer").style.visibility = "visible";
    document.getElementById("socialmediamodal").style.display = null;
}

const hideSocialMedia = () => {
    document.getElementById("modalcontainer").style.visibility = "hidden";
    document.getElementById("socialmediamodal").style.display = "none";
}

const GetAllGuides = () => {

    $.get("/guide/fishingspot").then((results) => {
        guidelist = results;

        const trs = guidelist.map((item, index) => {
            const tableElement = document.createElement("tr");

            const serviceText = item.fishingSpots.map((fishingSpot) => {
                return(fishingSpot.name);
            }).join("\n");

            addTextToTable(item.id, tableElement)
            addTextToTable(item.name, tableElement)
            addTextToTable(item.phone, tableElement)
            addTextToTable(item.email, tableElement)
            addTextToTable(item.bio, tableElement)
            addLinkToTable("view", () => {
                viewSocialMedia(item.id);
            }, "#", tableElement)
            addTextToTable(item.details, tableElement)
            addTextToTable(item.lat, tableElement)
            addTextToTable(item.lng, tableElement)
            addTextToTable(serviceText, tableElement)
            addImageToTable(item.image, tableElement);
            addLinkToTable("[edit]", () => {
                modifyGuideById(item.id)
            }, "#", tableElement);
            addLinkToTable("X", () => {
                deleteGuideById(item.id)
            }, "#", tableElement);

            return tableElement;
        })

        for(let tr of trs) {
            document.getElementById("infotablebody").appendChild(tr);
        }

    }).catch((e) => {
        console.log(e);
    })
}
