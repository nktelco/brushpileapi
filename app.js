let express = require('express');
let path = require('path');
let logger = require('morgan');
let mysql = require('mysql')
let uuid = require('uuid');
let fs = require('fs');
const Importer = require('mysql-import');
let crypto = require("crypto");

let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
let session = require('express-session');
let multer = require('multer');

//remove this
let authRouter = require('./routes/auth');

//routers
let indexRouter = require('./routes/index');
let setupRouter = require('./routes/setup');
let videoRouter = require('./routes/video');
let loginRouter = require('./routes/login');
let lakeRouter = require('./routes/lake');
let guideRouter = require('./routes/guide');
let fishingspotRouter = require('./routes/fishingspot');
let campaignRouter = require('./routes/campaign');
let sponsorRouter = require('./routes/sponsor');
let placeRouter = require('./routes/place');

let app = express();

//load your config
require('dotenv').config();

//database pool needs to be global
let dbPool;
//configure database pool

const db_config = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
};

const resetConnectionPool = () => {
    try {
        // get database pool
        dbPool = mysql.createPool(db_config);
    } catch(err) {
        console.log("error connecting to database " + err);
    }
}

//set up your initial connection pool
resetConnectionPool();

app.set("view engine", "ejs");
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use( bodyParser.raw() );
app.use( bodyParser.urlencoded({     // to support URL-encoded bodies
    limit: '50mb',
    extended: true
}));

const multerConfig = {
    dest: './images/',
    storage : multer.diskStorage({
        destination: function (req, file, next) {
            next(null,"./images/")
        },
        filename: function (req, file, next) {
            const filename = uuid.v4()
            const filePieces = (file.originalname.split("."));
            const fileextension = filePieces[filePieces.length-1].toLowerCase();
            next(null, filename + '.' + fileextension);
        }
    })
};

const multerSqlConfig = {
    dest: './sql/',
    storage : multer.diskStorage({
        destination: function (req, file, next) {
            next(null,"./sql/")
        },
        filename: function (req, file, next) {
            const filename = uuid.v4()
            const filePieces = (file.originalname.split("."));
            const fileextension = filePieces[filePieces.length-1].toLowerCase();
            next(null, filename + '.' + fileextension);
        }
    })
}

const upload = multer(multerConfig).single('file');
const uploadSql = multer(multerSqlConfig).single('file');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    genid: function(req) {
        return uuid.v1();  // use UUIDs for session IDs
    },
    secret: 'lbETq000Ins56e59neLohxMtzMZSn5Sl',
    resave: false,
    saveUninitialized: true,
    cookie:{}
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

//Depricated.  Use /video
app.use('/auth', authRouter);
app.use('/video', videoRouter);
app.use('/guide', (req, res, next) => {
    req.upload = upload;
    req.dbPool = dbPool;
    next();
}, guideRouter);

app.use('/lake', (req, res, next) => {
    req.upload = upload;
    req.dbPool = dbPool;
    next();
}, lakeRouter);

app.use('/fishingspot', (req, res, next) => {
    req.upload = upload;
    req.dbPool = dbPool;
    next();
}, fishingspotRouter);

app.use('/campaign', (req, res, next) => {
    req.upload = upload;
    req.dbPool = dbPool;
    next();
}, campaignRouter);

app.use('/sponsor', (req, res, next) => {
    req.upload = upload;
    req.dbPool = dbPool;
    next();
}, sponsorRouter);

app.use('/place', placeRouter);

app.use('/setup', (req, res, next) => {
    req.upload = uploadSql;
    req.dbPool = dbPool;
    req.db_config = db_config;
    req.resetConnectionPool = resetConnectionPool;
    next();
}, setupRouter);

app.use('/login', (req, res, next) => {
    req.dbPool = dbPool;
    next();
}, loginRouter);

app.use('/logout', (req, res, next) => {
    req.session.user_id = null;
    res.redirect("/login");
});

//handle admin
app.use('/admin', (req, res, next) => {
    if (!req.session.user_id) {
        res.redirect("/login");
    } else {
        next();
    }
})
app.use( '/admin', express.static("admin"));

//static route for images
app.use('/images', express.static("images"));

module.exports = app;
