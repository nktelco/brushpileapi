const express = require('express');
const router = express.Router();
const axios = require('axios');

//we shouldn't put this in code
const GOOGLE_API_KEY = "AIzaSyC2s_msHfbg7Lk8OYl5Y4SCHLR8o5FayDk";

router.get('/:text', (req, res, next) => {
    const {text} = req.params;
    //point_of_interest|landmark|colloquial_area

    const url=`https://maps.googleapis.com/maps/api/place/queryautocomplete/json` +
              `?fields=name` +
               `&input=${text}` +
               `&inputtype=textquery` +
               `&type=natural_feature` +
               `&key=${GOOGLE_API_KEY}`;

    axios({
        method: "GET", url: `${url}`
    }).then((result) => {
        res.send(result.data);
    }).catch((e) => {
        console.log(e);
        res.send([]);
    })
})

router.get('/id/:place_id', (req, res, next) => {
    const {place_id} = req.params;
    //point_of_interest|landmark|colloquial_area

    const url=`https://maps.googleapis.com/maps/api/place/details/json` +
        `?fields=geometry` +
        `&place_id=${place_id}` +
        `&key=${GOOGLE_API_KEY}`;

    axios({
        method: "GET", url: `${url}`
    }).then((result) => {
        res.send(result.data);
    }).catch((e) => {
        console.log(e);
        res.send([]);
    })
})

module.exports = router;