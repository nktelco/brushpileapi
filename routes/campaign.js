var express = require('express');
var router = express.Router();

const getCampaigns = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT c.id, c.sponsor_id, c.image, c.text, c.link, c.startdate, c.enddate, " +
                    "s.name as sponsor_name " +
                    "FROM campaign as c LEFT JOIN sponsor as s on c.sponsor_id = s.id where 1", (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const getCampaign = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT c.id, c.sponsor_id, c.image, c.text, c.link, c.startdate, c.enddate, " +
                    "s.name as sponsor_name " +
                    "FROM campaign as c LEFT JOIN sponsor as s on c.sponsor_id = s.id where c.id=?", [id], (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const deleteCampaignById = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("DELETE from campaign WHERE id=?", [id], (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}


const postCampaign = (dbPool, values) => {
    const { sponsor_id, image, text, link, startdate, enddate } = values;

    let st = null;
    let end = null;
    if(startdate) {
        st = new Date(startdate).toISOString().slice(0,-5).replace("T", " ");;
    }

    if(enddate) {
        end = new Date(enddate).toISOString().slice(0,-5).replace("T", " ");;
    }

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("INSERT INTO campaign (sponsor_id, image, text, link, startdate, enddate) " +
                    "VALUES (?, ?, ?, ?, ?, ?)", [
                    sponsor_id, image || null, text || "",
                    link || "", st || null, end || null
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results.insertId)
                    }
                })

                db.release();
            }
        })
    })
}

const patchCampaign = (dbPool, id, values) => {
    const { sponsor_id, image, text, link, startdate, enddate } = values;

    let st = null;
    let end = null;
    if(startdate) {
        st = new Date(startdate).toISOString().slice(0,-5).replace("T", " ");;
    }

    if(enddate) {
        end = new Date(enddate).toISOString().slice(0,-5).replace("T", " ");;
    }

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("UPDATE campaign SET " +
                    "sponsor_id=?, image=?, text=?, link=?, startdate=?, enddate=? " +
                    "WHERE id=?", [sponsor_id, image, text,
                        link, st || null, end || null, id],
                    (sqlerr, results) => {
                        if(sqlerr) {
                            reject(sqlerr)
                        } else {
                            resolve(id)
                        }
                    })

                db.release();
            }
        })
    })
}

const getCampaignByDate = (dbPool, startdate, enddate, limit = 3, page = 0) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("SELECT c.id, c.sponsor_id, c.image, c.text, c.link, c.startdate, c.enddate, " +
                    "s.name as sponsor_name " +
                    "FROM campaign as c LEFT JOIN sponsor as s on c.sponsor_id = s.id " +
                    "WHERE (c.startdate>? AND c.enddate<?) OR " +
                    "(c.startdate>? and c.enddate is null) OR " +
                    "(c.startdate IS null and c.enddate<?) OR " +
                    "(c.startdate IS null OR c.enddate IS null) " +
                    "ORDER BY c.startdate LIMIT ? OFFSET ?", [
                        startdate, enddate, startdate, enddate, limit, page*limit
                    ],
                    (sqlerr, results) => {
                        if(sqlerr) {
                            reject(sqlerr)
                        } else {
                            resolve(results)
                        }
                    })

                db.release();
            }
        })
    })
}

/* GET fishing spot. */
router.get('/', (req, res, next) => {
    getCampaigns(req.dbPool).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.get('/current/:numdays', (req, res, next) => {
    const {numdays} = req.params;
    const date = new Date();
    const startTimestamp = date.getTime();
    const endTimestamp = startTimestamp + (numdays * 24 * 60 * 60 * 1000);

    let page = Number(req.query.page) || 0;
    let limit = Number(req.query.limit) || 0;

    getCampaignByDate(req.dbPool, startTimestamp, endTimestamp, limit, page).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.get('/:id', (req, res, next) => {
    const {id} = req.params;

    getCampaign(req.dbPool, id).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.patch('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        patchCampaign(req.dbPool, id, req.body).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.delete('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        deleteCampaignById(req.dbPool, id).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }

})

router.post('/', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        postCampaign(req.dbPool, req.body).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.post('/image', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const upload = req.upload;
        upload(req, res, (err) => {
            if (err) {
                res.send(null);
            } else {
                res.send(req.file ? req.file.path : null);
            }
        })
    } else {
        res.sendStatus(401);
    }
})

module.exports = router;
