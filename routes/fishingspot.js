var express = require('express');
var router = express.Router();

const getFishingspots = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT fs.id, fs.name, fs.lat, fs.lng, fs.image, fs.lake_id, fs.city, fs.state, " +
                    "l.displayname as lakename " +
                    "FROM fishingspot as fs LEFT JOIN lake as l on lake_id = l.id where 1", (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const getFishingspot = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT fs.id, fs.name, fs.lat, fs.lng, fs.image, fs.lake_id, fs.city, fs.state, " +
                    "l.displayname as lakename " +
                    "FROM fishingspot as fs LEFT JOIN lake as l on lake_id = l.id where fs.id=?", [id], (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}


const postFishingspot = (dbPool, values) => {
    const { name, lat, lng, image, lake_id, city, state } = values;

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("INSERT INTO fishingspot (name, lat, lng, image, lake_id, city, state) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)", [
                    name, lat || null, lng || null, image, lake_id || null, city, state
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results.insertId)
                    }
                })

                db.release();
            }
        })
    })
}

const patchFishingspot = (dbPool, id, values) => {
    const { name, lat, lng, image, lake_id, city, state } = values;

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("UPDATE fishingspot SET " +
                        "name=?, lat=?, lng=?, image=?, lake_id=?, city=?, state=? " +
                        "WHERE id=?", [name, lat || null, lng || null,
                                       image, lake_id || null, city, state, id],
                (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr)
                    } else {
                        resolve(id)
                    }
                })

                db.release();
            }
        })
    })
}

const deleteFishingspot = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("DELETE from fishingspot WHERE id=?", [id],
                    (sqlerr, results) => {
                        if(sqlerr) {
                            reject(sqlerr)
                        } else {
                            resolve(results)
                        }
                    })

                db.release();
            }
        })
    })
}

/* GET fishing spot. */
router.get('/', (req, res, next) => {
    getFishingspots(req.dbPool).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.get('/:id', (req, res, next) => {
    const {id} = req.params;

    getFishingspot(req.dbPool, id).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.patch('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        patchFishingspot(req.dbPool, id, req.body).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.delete('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        deleteFishingspot(req.dbPool, id).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.post('/', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        postFishingspot(req.dbPool, req.body).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.post('/image', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const upload = req.upload;
        upload(req, res, (err) => {
            if (err) {
                res.send(null);
            } else {
                res.send(req.file ? req.file.path : null);
            }
        })
    } else {
        res.sendStatus(401);
    }
})

module.exports = router;
