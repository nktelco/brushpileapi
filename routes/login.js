var express = require('express');
var router = express.Router();
let crypto = require("crypto");

const checkUsernameAndPassword = (username, password, dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT id, password, salt, role_id FROM user " +
                    "WHERE username=? AND active=1", [username], (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        const user = results[0];
                        const hash = crypto.createHash('sha256').update(`${password}${user.salt}`).digest('hex');
                        if (hash === user.password) {
                            resolve(user);
                        } else {
                            reject("password");
                        }
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

//login
router.get('/', (req, res, next) => {
    //handle login
    if(!req.session.user_id) {
        next();
    } else {
        res.redirect("/admin")
    }
})

router.get('/', express.static("login"));
router.get('/css/*', express.static("login"));

router.post('/post', (req, res, next) => {
    let user = req.body.user;
    let password = req.body.password;
    let dbPool = req.dbPool;

    checkUsernameAndPassword(user, password, dbPool).then((result) => {
        req.session.user_id = result.id;
        res.redirect("/admin");
    }).catch((e) => {
        req.session.user_id = null;
        res.redirect("/login?err=login");
    })
})

module.exports = router;