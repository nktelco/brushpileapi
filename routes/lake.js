var express = require('express');
var router = express.Router();

const getLakes = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT * FROM lake where 1", (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const getLake = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT * FROM lake where id=?", [id], (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const postLake = (dbPool, values) => {
    const { displayname, image, lat, lng } = values;

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("INSERT INTO lake (displayname, image, lat, lng) VALUES (?, ?, ?, ?)", [
                    displayname, image, lat || null, lng || null
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results.insertId)
                    }
                })
            }
        })
    })
}

const deleteLake = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("DELETE from lake WHERE id=?", [id],
                    (sqlerr, results) => {
                        if(sqlerr) {
                            reject(sqlerr)
                        } else {
                            resolve(results)
                        }
                    })

                db.release();
            }
        })
    })
}

/* GET lake listing. */
router.get('/', (req, res, next) => {
    getLakes(req.dbPool).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

/* GET lake listing. */
router.get('/:id', (req, res, next) => {
    const {id} = req.params;

    getLake(req.dbPool, id).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.delete('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        deleteLake(req.dbPool, id).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.post('/', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        postLake(req.dbPool, req.body).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.post('/image', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const upload = req.upload;
        upload(req, res, (err) => {
            if (err) {
                res.send(null);
            } else {
                res.send(req.file ? req.file.path : null);
            }
        })
    } else {
        res.sendStatus(401);
    }
})

module.exports = router;
