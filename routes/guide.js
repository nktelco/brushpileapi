var express = require('express');
var router = express.Router();

const getGuides = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT * FROM guide where 1", (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const getGuidesWithFishingSpots = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT guide.id, guide.name, guide.phone, guide.email, guide.bio, guide.details, " +
                    "guide.lat, guide.lng, guide.image, fishingspot_id, fishingspot.name as fishingspot_name from guide " +
                    "LEFT JOIN guide_fishingspot ON guide_fishingspot.guide_id=guide.id " +
                    "LEFT JOIN fishingspot ON guide_fishingspot.fishingspot_id = fishingspot.id " +
                    "WHERE 1", (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const getGuideWithFishingSpots = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT guide.id, guide.name, guide.phone, guide.email, guide.bio, guide.details, " +
                    "guide.lat, guide.lng, guide.image, fishingspot_id, fishingspot.name as fishingspot_name from guide " +
                    "LEFT JOIN guide_fishingspot ON guide_fishingspot.guide_id=guide.id " +
                    "LEFT JOIN fishingspot ON guide_fishingspot.fishingspot_id = fishingspot.id " +
                    "WHERE guide.id=?", [id], (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const postGuide = (dbPool, values) => {
    const { name, phone, email, bio, details, lat, lng, image } = values;

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("INSERT INTO guide (name, phone, email, bio, details, lat, lng, image) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [
                    name, phone, email, bio, details, lat || null, lng || null, image
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results.insertId)
                    }
                })

                db.release();
            }
        })
    })
}

const patchGuide = (dbPool, values, id) => {
    const { name, phone, email, bio, details, lat, lng, image } = values;

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("UPDATE guide SET " +
                    "name=?, phone=?, email=?, bio=?, details=?, lat=?, lng=?, image=? " +
                    "WHERE id=?", [
                    name, phone, email, bio, details, lat || null, lng || null, image, id
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results.insertId)
                    }
                })

                db.release();
            }
        })
    })
}

const postSocialMediaLink = (dbPool, link, linktype, guide_id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("INSERT INTO link (guide_id, link, linktype) " +
                    "VALUES (?, ?, ?)", [
                    guide_id, link, linktype
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results.insertId)
                    }
                })

                db.release();
            }
        })
    })
}

const patchSocalMediaLink = (dbPool, link, linktype, link_id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("UPDATE link SET link=?, linktype=? WHERE id=?", [
                    link, linktype, link_id
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(link_id);
                    }
                })

                db.release();
            }
        })
    })
}

const getGuideById = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("SELECT guide.id as id, name, phone, email, bio, details, lat, lng, image, " +
                    "link.id as link_id, link, linktype from guide LEFT JOIN link ON guide.id=guide_id WHERE guide.id=?", [id], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                })

                db.release();
            } else {
                reject(err);
            }
        })
    })
}

const getGuideByName = (dbPool, name) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("SELECT * from guide WHERE lower(name)=lower(?)", [name], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results[0])
                    }
                })

                db.release();
            } else {
                reject(null);
            }
        })
    })
}

const getNearbyGuidesWithoutRadius = (dbPool, latitude, longitude, radius, limit, page) => {
    return new Promise((resolve, reject) => {
        getNearbyGuides(dbPool, latitude, longitude, radius, limit, page).then((result) => {
            resolve(result)
        }).catch((e) => {
            reject(e);
        });
    });
}

const getNearbyGuides = (dbPool, latitude, longitude, radius, optionalLimit = null, optionalPage= null) => {
    return new Promise((resolve, reject) => {
        const lng_min = longitude - radius / Math.abs(Math.cos(latitude * (Math.PI / 180)) * 69);
        const lng_max = longitude + (radius / Math.abs(Math.cos(latitude * (Math.PI / 180)) * 69));
        const lat_min = latitude - (radius / 69);
        const lat_max = latitude + ((radius / 69));

        dbPool.getConnection((err, db) => {

            const sub = [latitude, latitude, longitude, lng_min, lng_max, lat_min, lat_max, radius];
            const addLimit = (optionalLimit !== null && optionalPage !== null) ? `LIMIT ${optionalLimit} OFFSET ${optionalLimit * optionalPage}` : "";

            if (!err) {
                db.query("SELECT *, fishingspot.name AS fishingspot_name, " +
                    "((ACOS(SIN(? * PI() / 180) " +
                    "* SIN(fishingspot.lat * PI() / 180) + COS(? * PI() / 180) " +
                    "* COS(fishingspot.lat * PI() / 180) * COS((? - fishingspot.lng) " +
                    "* PI() / 180)) * 180 / PI()) * 60 * 1.1515) as distance " +
                    "FROM fishingspot " +
                    "JOIN guide_fishingspot ON fishingspot.id = guide_fishingspot.fishingspot_id "+
                    "JOIN guide ON guide_fishingspot.guide_id = guide.id "+
                    "WHERE (fishingspot.lng between ? AND ?) " +
                    "AND (fishingspot.lat BETWEEN ? and ?) " +
                    "HAVING distance < ? " +
                    "ORDER By distance " +
                    addLimit,
                    [
                        latitude, latitude, longitude, lng_min,
                        lng_max, lat_min, lat_max, radius
                    ],
                    (sqlerr, results) => {
                        if (sqlerr) {
                            reject(sqlerr);
                        } else {
                            resolve(results)
                        }
                    })

                db.release();
            } else {
                reject(null);
            }
        })
    })
}

const getGuidesByDetails = (dbPool, search) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("SELECT * from guide WHERE lower(details) LIKE lower(?)", [`%${search}%`], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                })

                db.release();
            } else {
                reject(null);
            }
        })
    })
}

const createGuideFishingSpot = (dbPool, guideId, fishingSpotId) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("INSERT INTO guide_fishingspot (guide_id, fishingspot_id) VALUES (?, ?)", [guideId, fishingSpotId], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                })

                db.release();
            } else {
                reject(null);
            }
        })
    })
}

const checkGuideFishingSpot = (dbPool, guideId, fishingSpotId) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("SELECT guide_id FROM guide_fishingspot WHERE guide_id=? AND fishingspot_id=?", [guideId, fishingSpotId], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                })

                db.release();
            } else {
                reject(null);
            }
        })
    })
}

const deleteGuideFishingSpot = (dbPool, guideId, fishingSpotId) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("DELETE FROM guide_fishingspot WHERE guide_id=? AND fishingspot_id=?", [guideId, fishingSpotId], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                })

                db.release();
            } else {
                reject(null);
            }
        })
    })
}

const deleteGuide = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("DELETE from guide WHERE id=?", [id],
                    (sqlerr, results) => {
                        if(sqlerr) {
                            reject(sqlerr)
                        } else {
                            resolve(results)
                        }
                    })

                db.release();
            }
        })
    })
}

/* GET users listing. */
router.get('/', (req, res, next) => {
    getGuides(req.dbPool).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.post('/', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        postGuide(req.dbPool, req.body).then((result) => {
            res.send(result.toString());
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.patch('/:id', (req, res, next) => {
    const id = req.params.id;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        patchGuide(req.dbPool, req.body, id).then((result) => {
            res.send(id.toString());
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

const addFishingSpotsToGuides = (result) => {
    return new Promise((resolve, reject) => {
        const resultWithFishingSpots = [];
        const spotids = {}
        for(const r of result) {
            if (spotids[r.id] == null) {
                r.fishingSpots = [];
                r.fishingSpots.push({id: r.fishingspot_id, name: r.fishingspot_name});
                spotids[r.id] = r;

                resultWithFishingSpots.push(spotids[r.id]);
            } else {
                spotids[r.id].fishingSpots.push({id: r.fishingspot_id, name: r.fishingspot_name});
            }
        }

        if(resultWithFishingSpots.length > 0) {
            resolve(resultWithFishingSpots);
        } else {
            reject();
        }
    })
}

router.get('/fishingspot', (req, res, next) => {
    getGuidesWithFishingSpots(req.dbPool).then(async (result) => {
        const resultWithFishingSpots = await addFishingSpotsToGuides(result);
        res.send(resultWithFishingSpots);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.get('/:id/fishingspot', (req, res, next) => {
    const {id} = req.params;
    getGuideWithFishingSpots(req.dbPool, id).then(async (result) => {
        const resultWithFishingSpots = await addFishingSpotsToGuides(result);
        res.send(resultWithFishingSpots);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
})

router.post('/fishingspot', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const {fishingSpotId, guideId} = req.body;

        checkGuideFishingSpot(req.dbPool, guideId, fishingSpotId).then((result) => {
            if(result.length === 0) {
                return (createGuideFishingSpot(req.dbPool, guideId, fishingSpotId))
            } else {
                return Promise.resolve();
            }
        }).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
})

router.delete('/fishingspot/:guideId/:fishingSpotId', (req, res, next) => {
    const {guideId, fishingSpotId} = req.params;

    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        deleteGuideFishingSpot(req.dbPool, guideId, fishingSpotId).then((resolve) => {
            res.sendStatus(200);
        }).catch((e) => {
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
})

router.delete('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        deleteGuide(req.dbPool, id).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.post('/link', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const {link, linktype, guide_id} = req.body;
        postSocialMediaLink(req.dbPool, link, linktype, guide_id).then((result) => {
            res.send(result.toString());
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
})

router.patch('/link/:link_id', (req, res, next) => {
    const link_id = req.params.link_id;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const {link, linktype} = req.body;
        patchSocalMediaLink(req.dbPool, link, linktype, link_id).then((result) => {
            res.send(result.toString())
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        });
    }
})

router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    if(id != null) {
        getGuideById(req.dbPool, id).then((result) => {
            if(result === null) {
                res.sendStatus(404);
            } else {
                res.send(result);
            }
        }).catch((e) => {
            res.status(500).send(e);
        })
    } else {
        res.sendStatus(404);
    }
})

router.get('/name/:name', (req, res, next) => {
    const name = req.params.name;
    if(name != null) {
        getGuideByName(req.dbPool, name).then((result) => {
            if(result === null) {
                res.sendStatus(404);
            } else {
                res.send(result);
            }
        }).catch((e) => {
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(404);
    }
})

router.get('/details/:search', (req, res, next) => {
    const search = req.params.search;
    if(search != null) {
        getGuidesByDetails(req.dbPool, search).then((result) => {
            res.send(result);
        }).catch((e) => {
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(404);
    }
})

router.post('/image', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const upload = req.upload;
        upload(req, res, (err) => {
            if (err) {
                res.send(null);
            } else {
                res.send(req.file ? req.file.path : null);
            }
        })
    } else {
        res.sendStatus(401);
    }
})

router.get('/distance/:lat/:lng/:radius', (req, res, next) => {
    const {lat, lng, radius} = req.params;

    if(lat != null && lng != null && radius != null) {
        getNearbyGuides(req.dbPool, parseFloat(lat), parseFloat(lng), parseFloat(radius)).then((result) => {
            res.send(result);
        }).catch((e) => {
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(404);
    }
})

router.get('/nearest/:lat/:lng', (req, res, next) => {
    const {lat, lng} = req.params;
    const limit = req.query.limit || 5;
    const page = req.query.page || 0;
    const radius = req.query.radius || 500;

    if(lat != null && lng != null) {
        getNearbyGuidesWithoutRadius(req.dbPool, parseFloat(lat), parseFloat(lng), parseInt(radius), parseInt(limit), parseInt(page)).then((result) => {
            res.send(result);
        }).catch((e) => {
            console.log(e);
            res.status(500).send(e);
        })
    } else {
        res.sendStatus(404);
    }
})

module.exports = router;
