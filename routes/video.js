var express = require('express');
var router = express.Router();
let crypto = require('crypto');
let axios = require('axios');
var base64 = require('base-64');
const FormData = require('form-data');

const applicationId = 33;
const baseHost = `https://live.nkt.tv`;
const baseUrl = `/api/${applicationId}`;
const apiKey = "QvyWl12J0dmN357M";
const authKeyGuest = "<guest>";
const user="appDev";
const pass="BPF123";

/* GET users listing. */
router.get('/channels', function(req, res, next) {

    Auth().then((authResult) => {
        return getChannelList(authResult);
    }).then((result) => {
        res.send(result.data);
    }).catch((e) => {
        res.send(e);
    });
});

const getChannelList = (auth) => {
    return new Promise((resolve, reject) => {
        const authKey = auth.api_auth_key;

        // The base URL to use.
        const urlToSign = `${baseUrl}/channels?live=1`;
        const basicAuthPassword = SignUrl("GET", urlToSign, authKey)

        axios({
            method: "GET", url: `${baseHost}${urlToSign}`, auth: {
                username: authKey,
                password: basicAuthPassword
            }
        }).then((result) => {
            resolve(result)
        }).catch((e) => {
            reject(e);
        })
    })
}

router.get('/videos/live', function(req, res, next) {
    let authResult;
    let channelId;
    Auth().then((result) => {
        authResult = result;
        return (getChannelList(authResult))
    }).then((result) => {
        const filteredList = result.data.channels.filter((r) => {
            return (r.name == "BrushPile Fishing" && r.channel_type == "live_channel");
        })
        channelId = filteredList[0].id;

        const authKey = authResult.api_auth_key;
        const urlToSign = `${baseUrl}/channels/${channelId}/external-stream-url`
        const basicAuthPassword = SignUrl("GET", urlToSign, authKey)
        return (
            axios({
                method: "GET", url: `${baseHost}${urlToSign}`, auth: {
                    username: authKey,
                    password: basicAuthPassword
                }
            })
        )
    }).then((result) => {
        res.send(result.data);
    }).catch((e) => {
        console.log("ERROR * * * * * **** *");
        console.log(e);
    })
});

router.get('/videos/:videoId', function(req, res, next) {

    Auth().then((result) => {
        const authKey = result.api_auth_key;
        const videoId = req.params.videoId

        // The base URL to use.
        const urlToSign = `${baseUrl}/videos/${videoId}`;
        const basicAuthPassword = SignUrl("GET", urlToSign, authKey)

        return (
            axios({
                method: "GET", url: `${baseHost}${urlToSign}`, auth: {
                    username: authKey,
                    password: basicAuthPassword
                }
            })
        )
    }).then((result) => {
        res.send(result.data);
    }).catch((e) => {
        res.send(e);
    });
});


const SignUrl = (method, urlToSign, authKey) => {
    const signString = `${method.toUpperCase()}:${urlToSign}:${authKey}`;

    const hash = crypto.createHmac("sha256", apiKey)
        .update(signString)
        .digest('hex')

    // User authentication to the API occurs via HTTP Basic Auth.
    // Provide your authentication key (see section AUTHENTICATE) as the basic auth username.
    // Provide your authentication token (generated below) as the basic auth password.
    // You must authenticate for all requests.
    return hash;
}

const Auth = () => {
    return new Promise((resolve, reject) => {
        const authUrl = `${baseUrl}/authenticate`;
        const md5hash = crypto.createHash("md5")
            .update(pass)
            .digest('hex')

        const formData = new FormData();
        formData.append('username', user);
        formData.append('password', md5hash);

        console.log("md5", md5hash);

        const basicAuthPassword = SignUrl("POST", authUrl, '<guest>');
        const headers = {
            "Content-Type": `multipart/form-data; boundary=${formData.getBoundary()}`
        };

        axios({
            method: "POST",
            url: `${baseHost}${authUrl}`,
            auth: {
                username: "<guest>",
                password: basicAuthPassword
            },
            headers,
            data: formData
        })
            .then((result) => {
                resolve(result.data);
            }).catch((e) => {
            console.log(e);
            reject(e);
        })
    });
}

module.exports = router;
