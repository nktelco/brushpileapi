var express = require('express');
var router = express.Router();

const getSponsors = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT * FROM sponsor where 1", (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results)
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const getSponsor = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("SELECT * FROM sponsor where id=?", [id], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results);
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const postSponsor = (dbPool, values) => {
    const { name, contact, contactemail } = values;

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("INSERT INTO sponsor (name, contact, contactemail) " +
                    "VALUES (?, ?, ?)", [
                    name, contact, contactemail
                ], (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve(results.insertId)
                    }
                })

                db.release();
            }
        })
    })
}

const patchSponsor = (dbPool, id, values) => {
    const { name, contact, contactemail } = values;

    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("UPDATE sponsor SET " +
                    "name=?, contact=?, contactemail=? " +
                    "WHERE id=?", [name, contact || null, contactemail || null, id],
                    (sqlerr, results) => {
                        if(sqlerr) {
                            reject(sqlerr)
                        } else {
                            resolve(id)
                        }
                    })

                db.release();
            }
        })
    })
}

const deleteSponsor = (dbPool, id) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if(!err) {
                db.query("DELETE from sponsor WHERE id=?", [id],
                (sqlerr, results) => {
                    if(sqlerr) {
                        reject(sqlerr)
                    } else {
                        resolve(results)
                    }
                })

                db.release();
            }
        })
    })
}

router.get('/', (req, res, next) => {
    getSponsors(req.dbPool).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.get('/:id', (req, res, next) => {
    const {id} = req.params;

    getSponsor(req.dbPool, id).then((result) => {
        res.send(result);
    }).catch((e) => {
        console.log(e);
        res.sendStatus(404);
    })
});

router.post('/', (req, res, next) => {
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        console.log(req.body);
        postSponsor(req.dbPool, req.body).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
})

router.patch('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        patchSponsor(req.dbPool, id, req.body).then((result) => {
            res.sendStatus(200);
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
});

router.delete('/:id', (req, res, next) => {
    const {id} = req.params;
    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        deleteSponsor(req.dbPool, id).then((result) => {
            req.sendStatus(200)
        }).catch((e) => {
            console.log(e);
            res.sendStatus(500);
        })
    } else {
        res.sendStatus(401);
    }
})

module.exports = router;
