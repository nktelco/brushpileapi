const express = require('express');
const crypto = require("crypto");
const mysql = require('mysql');
const Importer = require('mysql-import');
const mysqldump = require('mysqldump');

const router = express.Router();
const database_name = "fishbook"

const checkDB = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT count(*) as ct from user where 1", (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        console.log("GOT RESULTS: ", results);

                        if(results.length == 0) {
                            reject();
                        } else if(results[0].ct == 0) {
                            reject();
                        } else {
                            resolve(results.length)
                        }
                    }
                });

                db.release();
            } else {
                reject();
            }
        })
    })
}

const dropOldDatabase = (dbPool) => {
    return new Promise((resolve, reject) => {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query(`DROP DATABASE ${database_name}`, (sqlerr, results) => {
                    resolve();
                });

                db.release();
            } else {
                console.log("There was no database to drop");
                resolve();
            }
        })
    })
}

const createDatabase = () => {
    return new Promise((resolve, reject) => {
        const conf = {
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
        };

        const con = mysql.createConnection(conf);
        con.connect((err) => {
            if (!err) {
                con.query(`CREATE DATABASE ${database_name}`, (sqlerr, results) => {
                    if (sqlerr) {
                        reject(sqlerr);
                    } else {
                        resolve();
                    }
                });
                con.end();

            } else {
                console.log("encountered an error creating database");
                reject(err);
            }
        })
    })
}

const setupDatabase = (db_config, sql_file) => {
    return new Promise((resolve, reject) => {

        const useSql = sql_file || "fishbook_dump.sql";

        let percent = 0;
        const importer = new Importer(db_config);
        importer.onProgress(progress => {
            let pct = Math.floor(progress.bytes_processed / progress.total_bytes * 10000) / 100;
            console.log(progress.total_bytes);
            percent = pct;
            console.log(`${percent}% Completed`);
        });

        importer.import(`./sql/${useSql}`).then(() => {
            let files_imported = importer.getImported();
            console.log(`${files_imported.length} SQL file(s) imported.`);
            if(percent >= 100) {
                resolve();
            }
        }).catch(err => {
            reject(err);
        });
    });
}

const createBaseAdminUser = (dbPool) => {
    return new Promise((resolve, reject) => {

        const username = "admin";
        const password = crypto.randomBytes(8).toString('hex');
        const salt = crypto.randomBytes(32).toString('hex');
        const hash = crypto.createHash('sha256').update(`${password}${salt}`).digest('hex');
        const role_id = 9;

        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("INSERT into user " +
                    "(username, password, salt, role_id, createdOn, active) " +
                    "VALUES " +
                    "(?, ?, ?, ?, now(), ?)", [username, hash, salt, role_id, 1], (sqlerr, results) => {
                    if(!sqlerr) {
                        resolve({username, password});
                    } else {
                        reject(sqlerr);
                    }
                });

                db.release();
            } else {
                console.log("user could not be created");
                reject();
            }
        })
    })
}

const checkAdminUser = (dbPool) => {
    return new Promise((resolve, reject) =>
    {
        dbPool.getConnection((err, db) => {
            if (!err) {
                db.query("SELECT username FROM `user` WHERE 1", (sqlerr, results) => {
                    if (!sqlerr) {
                        resolve(results);
                    } else {
                        reject(sqlerr);
                    }
                });

                db.release();
            } else {
                console.log("user could not be created");
                reject();
            }
        })
    })
}

const setupDatabaseAndInsertTempUser = (dbPool, db_config, resetConnectionPool) => {
    return new Promise((resolve, reject) => {
        dropOldDatabase(dbPool).then(() => {
            return (createDatabase());
        }).then(() => {
            return (setupDatabase(db_config));
        }).then(() => {
            resetConnectionPool();
            checkAdminUser(dbPool).then((result) => {
                if(result.length > 0) {
                    resolve(null);
                } else {
                    return (createBaseAdminUser(dbPool));
                }
            }).catch((e) => {
                //we don't have a db pool
                resolve(null);
            })
        }).then((result) => {
            console.log("Database set up");
            resolve(result);
        }).catch((err) => {
            console.log(err);
            reject();
        })
    });
}

router.get('/recreate', (req, res, next) => {
    const dbPool = req.dbPool;
    const resetConnectionPool = req.resetConnectionPool;

    if(!req.session.user_id) {
        res.sendStatus(500);
    } else {
        dropOldDatabase(dbPool).then(() => {
            resetConnectionPool();
            res.sendStatus(200);
        }).catch((e) => {
            res.sendStatus(500);
        })
    }
});

router.get('/dbdump', async(req, res, next) => {
    if(!req.session.user_id) {
        res.sendStatus(403);
    } else {
        try {
            const result = await mysqldump({
                connection: {
                    host: process.env.DB_HOST,
                    user: process.env.DB_USER,
                    password: process.env.DB_PASSWORD,
                    database: database_name,
                }
            });
            res.setHeader('Content-disposition', 'attachment; filename=fishbook_dump.sql');
            res.setHeader('Content-type', 'text/plain');
            res.send(result);
        } catch (e) {
            res.status(500).send(e);
        }
    }
});

router.post('/upload', (req, res, next) => {
    const dbPool = req.dbPool;
    const resetConnectionPool = req.resetConnectionPool;
    const db_config = req.db_config;

    if(req.session.user_id !== null && req.session.user_id !== undefined) {
        const upload = req.upload;
        upload(req, res, (err) => {
            if (err) {
                res.send(null);
            } else {
                const filePath = req.file ? req.file.path : null;
                if(filePath != null) {
                    dropOldDatabase(dbPool).then(() => {
                        resetConnectionPool();
                        return(setupDatabase(db_config, req.file));
                    }).then((result) => {
                        res.sendStatus(200);
                    }).catch((e) => {
                        res.sendStatus(500);
                    })
                } else {
                    res.sendStatus(500);
                }
            }
        })
    } else {
        res.sendStatus(401);
    }
})

router.get('/', (req, res, next) => {
    const dbPool = req.dbPool;
    const db_config = req.db_config;
    const resetConnectionPool = req.resetConnectionPool;

    checkDB(dbPool).then(() => {
        next();
    }).catch(() => {
        setupDatabaseAndInsertTempUser(dbPool, db_config, resetConnectionPool).then((result) => {
            if(result) {
                const {username, password} = result;

                res.render("./setupAdmin/setupAdmin", {
                    username,
                    password
                });
            } else {
                res.render("./setupAdmin/returnToLogin", null)
            }
        }).catch((e) => {
            res.status(500).send(e);
        });
    })
})

router.get('/', (req, res) => {
    res.redirect('/admin');
})

module.exports = router;
